# Industrial Cooling Management App
## Introduction
Welcome to the Industrial Cooling Management App! This application is designed to efficiently manage and monitor industrial cooling systems. It is built using Java for the backend and utilizes the H2 database with SQL for data storage. This README file provides essential information to help you set up, configure, and use the application.
## Prerequisites
Before getting started, ensure that you have the following prerequisites installed:
- Java Development Kit (JDK) 21 or later
- Apache Maven
- H2 Database
## Installation
1. Clone the repository to your local machine by running the following command in your terminal.
   ```bash
   git clone https://gitlab.com/bornasinovcic/industrial-cooling-management-app.git
   ```
2. Open the cloned repository using IntelliJ IDEA.
3. Download the H2 database ZIP from [H2 Database Download Page](https://www.h2database.com/html/download.html).
4. Start the H2 database using the credentials specified in the [database.properties](files/database.properties) file.
5. Import the initial data by executing the SQL script [main.sql](main.sql) into the database.
6. Execute the main function found within the [Main](src/main/java/com/example/industrial_cooling_management_app/main/Main.java) class.
# License
This project is licensed under the [MIT License](https://opensource.org/license/mit/). Feel free to modify and distribute it according to the terms of the license.