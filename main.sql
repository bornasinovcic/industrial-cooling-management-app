DROP TABLE IF EXISTS CoolingSystem, IndustrialFacility, Manufacturer;
CREATE TABLE Manufacturer (
    ManufacturerId   LONG        NOT NULL GENERATED ALWAYS AS IDENTITY,
    ManufacturerName VARCHAR(40) NOT NULL,
    PRIMARY KEY      (ManufacturerId),
    UNIQUE           (ManufacturerName)
);
CREATE TABLE IndustrialFacility (
    IndustrialFacilityId          LONG          NOT NULL GENERATED ALWAYS AS IDENTITY,
    IndustrialFacilityName        VARCHAR(40)   NOT NULL,
    IndustrialFacilityTemperature DECIMAL(5, 2) NOT NULL,
    PRIMARY KEY                   (IndustrialFacilityId)
);
CREATE TABLE CoolingSystem (
    CoolingSystemId               LONG          NOT NULL GENERATED ALWAYS AS IDENTITY,
    CoolingSystemName             VARCHAR(40)   NOT NULL,
    ManufacturerId                LONG          NOT NULL,
    FacilityId                    LONG          NOT NULL,
    CoolingSystemPrice            DECIMAL(7, 2) NOT NULL,
    CoolingSystemElectricityUsage DECIMAL(7, 2) NOT NULL,
    CoolingSystemTemperatureDrop  DECIMAL(5, 2) NOT NULL,
    CoolingSystemOnOrOff          BOOLEAN       NOT NULL,
    PRIMARY KEY                   (CoolingSystemId),
    FOREIGN KEY                   (ManufacturerId) REFERENCES Manufacturer (ManufacturerId),
    FOREIGN KEY                   (FacilityId)     REFERENCES IndustrialFacility (IndustrialFacilityId)
);
INSERT INTO Manufacturer (ManufacturerName)
VALUES ('Airedale International'),
('Baltimore Aircoil'),
('Brentwood Industries'),
('Emerson'),
('Johnson Controls'),
('Schneider Electric'),
('SPX'),
('GEA Group'),
('Rittal'),
('EVAPCO');
INSERT INTO IndustrialFacility (IndustrialFacilityName, IndustrialFacilityTemperature)
VALUES ('Facility01', 54.77),
('Facility02', 39.09),
('Facility03', 30.87),
('Facility04', 37.26),
('Facility05', 54.84),
('Facility06', 54.17),
('Facility07', 40.26),
('Facility08', 39.21),
('Facility09', 32.37),
('Facility10', 41.94);
INSERT INTO CoolingSystem (CoolingSystemName, ManufacturerId, FacilityId, CoolingSystemPrice, CoolingSystemElectricityUsage, CoolingSystemTemperatureDrop, CoolingSystemOnOrOff)
VALUES ('System01', 1, 6, 6948.03, 70.35, 1.37, TRUE),
('System02', 2, 2, 1375.97, 88.07, 5.48, TRUE),
('System03', 2, 6, 3499.95, 73.84, 2.07, TRUE),
('System04', 5, 2, 4472.8, 101.38, 1.7, TRUE),
('System05', 5, 6, 5749.01, 64.47, 6.25, TRUE),
('System06', 5, 2, 3693.02, 55.57, 5.58, TRUE),
('System07', 7, 1, 4109.40, 90.86, 4.21, TRUE),
('System08', 7, 1, 4132.91, 69.19, 3.16, TRUE),
('System09', 7, 1, 7354.72, 76.95, 8.83, TRUE),
('System10', 7, 1, 1631.04, 117.12, 7.25, TRUE),
('System11', 9, 5, 8352.88, 65.48, 3.1, TRUE),
('System12', 9, 10, 5212.61, 75.17, 3.44, TRUE),
('System13', 9, 6, 2747.23, 62.98, 2.54, TRUE),
('System14', 9, 5, 1415.94, 86.88, 9.99, TRUE),
('System15', 9, 6, 2110.59, 99.14, 3.37, TRUE);
SELECT * FROM Manufacturer;

SELECT * FROM IndustrialFacility;

SELECT CoolingSystemId,
    CoolingSystemName,
    ManufacturerId,
    FacilityId,
    CoolingSystemPrice,
    CoolingSystemElectricityUsage,
    CoolingSystemTemperatureDrop,
    CASE WHEN CoolingSystemOnOrOff = TRUE THEN 'ON' ELSE 'OFF' END AS CoolingSystemOnOrOff
FROM CoolingSystem;

SELECT * FROM CoolingSystem;

SELECT Manufacturer.*, COUNT(CoolingSystemId) AS numberOfSoldUnits
FROM Manufacturer LEFT JOIN CoolingSystem ON CoolingSystem.ManufacturerId = Manufacturer.ManufacturerId
GROUP BY Manufacturer.ManufacturerId
ORDER BY numberOfSoldUnits DESC, ManufacturerName;

SELECT IndustrialFacility.*,
    COUNT(CoolingSystemId) AS numberOfSystems,
    COALESCE(SUM(CoolingSystemPrice), 0) AS sumOfPrices, -- COALESCE == ISNULL
    SUM(CASE WHEN CoolingSystem.CoolingSystemOnOrOff = TRUE THEN CoolingSystemElectricityUsage ELSE 0 END) AS sumOfElectricityUsage,
    SUM(CASE WHEN CoolingSystem.CoolingSystemOnOrOff = TRUE THEN CoolingSystemTemperatureDrop ELSE 0 END) AS sumOfTemperatureDrop,
    TRIM(TRAILING '0' FROM
        IndustrialFacilityTemperature - (IndustrialFacilityTemperature * (SUM(CASE WHEN CoolingSystem.CoolingSystemOnOrOff = TRUE THEN CoolingSystemTemperatureDrop ELSE 0 END)  / 100))
    ) AS temperatureAfterDrop
FROM IndustrialFacility LEFT JOIN CoolingSystem ON CoolingSystem.FacilityId = IndustrialFacility.IndustrialFacilityId
GROUP BY IndustrialFacility.IndustrialFacilityId
ORDER BY numberOfSystems DESC;