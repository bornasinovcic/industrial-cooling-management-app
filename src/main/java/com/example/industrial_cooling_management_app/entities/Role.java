package com.example.industrial_cooling_management_app.entities;

public enum Role {
    ADMIN, USER
}
