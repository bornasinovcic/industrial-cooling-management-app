package com.example.industrial_cooling_management_app.entities;

import com.example.industrial_cooling_management_app.exceptions.CheckBoxNotSelectedException;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;

public class PasswordGenerator {
    public static String randomPasswordGenerator(CheckBox upperCase, CheckBox lowerCase, CheckBox numbers, int length) {
        StringBuilder passwordBuilder = new StringBuilder(length);
        String includedCharacters = "";

        String includeUpper = "ABCDEFGHIJKLMNOPQRSTUVWXY";
        String includeLower = "abcdefghijklmnopqrstuvxyz";
        String includeNumbers = "0123456789";

        if (!upperCase.isSelected() && !lowerCase.isSelected() && !numbers.isSelected()) {
            throw new CheckBoxNotSelectedException("You have to select at least one checkbox for password to be generated!");
        }

        includedCharacters += upperCase.isSelected() ? includeUpper : "";
        includedCharacters += lowerCase.isSelected() ? includeLower : "";
        includedCharacters += numbers.isSelected() ? includeNumbers : "";

        for (int i = 0; i < length; i++) {
            int index = (int) (includedCharacters.length() * Math.random());
            passwordBuilder.append(includedCharacters.charAt(index));
        }

        return passwordBuilder.toString();
    }
}
