package com.example.industrial_cooling_management_app.entities;

public class Manufacturer extends Id {

    private String manufacturerName;
    private Integer numberOfSoldUnits;

    public Manufacturer(Long entityId, String manufacturerName, Integer numberOfSoldUnits) {
        super(entityId);
        this.manufacturerName = manufacturerName;
        this.numberOfSoldUnits = numberOfSoldUnits;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public Integer getNumberOfSoldUnits() {
        return numberOfSoldUnits;
    }

    public void setNumberOfSoldUnits(Integer numberOfSoldUnits) {
        this.numberOfSoldUnits = numberOfSoldUnits;
    }

    @Override
    public String toString() {
        return getEntityId() + ", " + manufacturerName;
    }
}
