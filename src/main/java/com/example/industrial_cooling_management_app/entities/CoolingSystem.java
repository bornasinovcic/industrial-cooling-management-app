package com.example.industrial_cooling_management_app.entities;

import java.math.BigDecimal;

public class CoolingSystem extends Id {
    private String coolingSystemName;
    private Long manufacturerId, facilityId;
    private BigDecimal price, electricityUsage, temperatureDrop;
    private Boolean status;

    public CoolingSystem(Long entityId) {
        super(entityId);
    }

    public CoolingSystem(Long entityId, String coolingSystemName, Long manufacturerId, Long facilityId, BigDecimal price, BigDecimal electricityUsage, BigDecimal temperatureDrop, Boolean status) {
        super(entityId);
        this.coolingSystemName = coolingSystemName;
        this.manufacturerId = manufacturerId;
        this.facilityId = facilityId;
        this.price = price;
        this.electricityUsage = electricityUsage;
        this.temperatureDrop = temperatureDrop;
        this.status = status;
    }

    public String getCoolingSystemName() {
        return coolingSystemName;
    }

    public void setCoolingSystemName(String coolingSystemName) {
        this.coolingSystemName = coolingSystemName;
    }

    public Long getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Long manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public Long getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getElectricityUsage() {
        return electricityUsage;
    }

    public void setElectricityUsage(BigDecimal electricityUsage) {
        this.electricityUsage = electricityUsage;
    }

    public BigDecimal getTemperatureDrop() {
        return temperatureDrop;
    }

    public void setTemperatureDrop(BigDecimal temperatureDrop) {
        this.temperatureDrop = temperatureDrop;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return getEntityId() + ", " + coolingSystemName + ", " + price;
    }
}
