package com.example.industrial_cooling_management_app.entities;

public class User extends Id {
    private String userName, userPassword;
    private Role role;

    public User(Long entityId, String userName, String userPassword, Role role) {
        super(entityId);
        this.userName = userName;
        this.userPassword = userPassword;
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public static class UserBuilder {
        private Long entityId;
        private String userName;
        private String userPassword;
        private Role role;

        public UserBuilder setEntityId(Long entityId) {
            this.entityId = entityId;
            return this;
        }

        public UserBuilder setUserName(String userName) {
            this.userName = userName;
            return this;
        }

        public UserBuilder setUserPassword(String userPassword) {
            this.userPassword = userPassword;
            return this;
        }

        public UserBuilder setRole(Role role) {
            this.role = role;
            return this;
        }

        public User createUser() {
            return new User(entityId, userName, userPassword, role);
        }
    }

    @Override
    public String toString() {
        return getEntityId() + ". " + userName + " -> " + role;
    }
}
