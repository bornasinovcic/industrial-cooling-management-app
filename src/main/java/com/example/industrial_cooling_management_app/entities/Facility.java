package com.example.industrial_cooling_management_app.entities;

import java.math.BigDecimal;

public class Facility extends Id {
    private String name;

    private BigDecimal temperatureBefore, sumoOfPrices, sumOfElectricityUsage, sumOfTemperatureDrop, temperatureAfter;
    private Integer numberOfUnits;
    public Facility(Long entityId) {
        super(entityId);
    }

    public Facility(Long entityId, String name, BigDecimal temperatureBefore, BigDecimal sumoOfPrices, BigDecimal sumOfElectricityUsage, BigDecimal sumOfTemperatureDrop, BigDecimal temperatureAfter, Integer numberOfUnits) {
        super(entityId);
        this.name = name;
        this.temperatureBefore = temperatureBefore;
        this.sumoOfPrices = sumoOfPrices;
        this.sumOfElectricityUsage = sumOfElectricityUsage;
        this.sumOfTemperatureDrop = sumOfTemperatureDrop;
        this.temperatureAfter = temperatureAfter;
        this.numberOfUnits = numberOfUnits;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getTemperatureBefore() {
        return temperatureBefore;
    }

    public void setTemperatureBefore(BigDecimal temperatureBefore) {
        this.temperatureBefore = temperatureBefore;
    }

    public BigDecimal getSumoOfPrices() {
        return sumoOfPrices;
    }

    public void setSumoOfPrices(BigDecimal sumoOfPrices) {
        this.sumoOfPrices = sumoOfPrices;
    }

    public BigDecimal getSumOfElectricityUsage() {
        return sumOfElectricityUsage;
    }

    public void setSumOfElectricityUsage(BigDecimal sumOfElectricityUsage) {
        this.sumOfElectricityUsage = sumOfElectricityUsage;
    }

    public BigDecimal getSumOfTemperatureDrop() {
        return sumOfTemperatureDrop;
    }

    public void setSumOfTemperatureDrop(BigDecimal sumOfTemperatureDrop) {
        this.sumOfTemperatureDrop = sumOfTemperatureDrop;
    }

    public BigDecimal getTemperatureAfter() {
        return temperatureAfter;
    }

    public void setTemperatureAfter(BigDecimal temperatureAfter) {
        this.temperatureAfter = temperatureAfter;
    }

    public Integer getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(Integer numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    @Override
    public String toString() {
        return getEntityId() + ", " + name + ", " + temperatureBefore;
//        this.temperatureBefore = temperatureBefore;
//        this.sumoOfPrices = sumoOfPrices;
//        this.sumOfElectricityUsage = sumOfElectricityUsage;
//        this.sumOfTemperatureDrop = sumOfTemperatureDrop;
//        this.temperatureAfter = temperatureAfter;
//        this.numberOfUnits = numberOfUnits;

    }
}
