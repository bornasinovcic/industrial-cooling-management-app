package com.example.industrial_cooling_management_app.entities;

import java.io.Serializable;

public abstract class Id implements Serializable {
    private Long entityId;

    public Id(Long entityId) {
        this.entityId = entityId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }
}
