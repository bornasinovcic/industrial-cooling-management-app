package com.example.industrial_cooling_management_app.database;

import com.example.industrial_cooling_management_app.entities.CoolingSystem;
import com.example.industrial_cooling_management_app.entities.Facility;
import com.example.industrial_cooling_management_app.entities.Manufacturer;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class DatabaseHandling {

    private static Connection connectingToDatabase() throws IOException, SQLException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("files/database.properties"));
        return DriverManager.getConnection(properties.getProperty("link"), properties.getProperty("name"), properties.getProperty("password"));
    }

    public static List<Manufacturer> getAllManufacturers() {
        List<Manufacturer> manufacturers = new ArrayList<>();
        try (Connection connection = connectingToDatabase()) {
            if (connection != null) {
                System.out.println("Connected to database.");
                String query = """
                        SELECT Manufacturer.*, COUNT(CoolingSystemId) AS numberOfSoldUnits
                        FROM Manufacturer LEFT JOIN CoolingSystem ON CoolingSystem.ManufacturerId = Manufacturer.ManufacturerId
                        GROUP BY Manufacturer.ManufacturerId;""";
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                while (resultSet.next()) {
                    Long entityId = resultSet.getLong("ManufacturerId");
                    String entityName = resultSet.getString("ManufacturerName");
                    Integer numberOfSoldUnits = resultSet.getInt("numberOfSoldUnits");
                    manufacturers.add(new Manufacturer(entityId, entityName, numberOfSoldUnits));
                }
            }
        } catch (Exception e) {
            System.out.println("There was a problem connecting to database.");
        }
        return manufacturers;
    }

    public static List<CoolingSystem> getAllCoolingSystem() {
        List<CoolingSystem> coolingSystems = new ArrayList<>();
        try (Connection connection = connectingToDatabase()) {
            if (connection != null) {
                System.out.println("Connected to database.");
                String query = "SELECT * FROM CoolingSystem;";
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                while (resultSet.next()) {
                    Long entityId = resultSet.getLong("CoolingSystemId");
                    String entityName = resultSet.getString("CoolingSystemName");
                    Long manufacturerIdId = resultSet.getLong("ManufacturerId");
                    Long facilityId = resultSet.getLong("FacilityId");
                    BigDecimal price = resultSet.getBigDecimal("CoolingSystemPrice");
                    BigDecimal electricityUsage = resultSet.getBigDecimal("CoolingSystemElectricityUsage");
                    BigDecimal temperatureDrop = resultSet.getBigDecimal("CoolingSystemTemperatureDrop");
                    Boolean status = resultSet.getBoolean("CoolingSystemOnOrOff");
                    coolingSystems.add(new CoolingSystem(entityId, entityName, manufacturerIdId, facilityId, price, electricityUsage, temperatureDrop, status));
                }
            }
        } catch (Exception e) {
            System.out.println("There was a problem connecting to database.");
        }
        return coolingSystems;
    }

    public static List<Facility> getAllFacilities() {
        List<Facility> facilities = new ArrayList<>();
        try (Connection connection = connectingToDatabase()) {
            if (connection != null) {
                System.out.println("Connected to database.");
                String query = """
                        SELECT IndustrialFacility.*,
                            COUNT(CoolingSystemId) AS numberOfSystems,
                            COALESCE(SUM(CoolingSystemPrice), 0) AS sumOfPrices, -- COALESCE == ISNULL
                            SUM(CASE WHEN CoolingSystem.CoolingSystemOnOrOff = TRUE THEN CoolingSystemElectricityUsage ELSE 0 END) AS sumOfElectricityUsage,
                            SUM(CASE WHEN CoolingSystem.CoolingSystemOnOrOff = TRUE THEN CoolingSystemTemperatureDrop ELSE 0 END) AS sumOfTemperatureDrop,
                            TRIM(TRAILING '0' FROM
                                IndustrialFacilityTemperature - (IndustrialFacilityTemperature * (SUM(CASE WHEN CoolingSystem.CoolingSystemOnOrOff = TRUE THEN CoolingSystemTemperatureDrop ELSE 0 END)  / 100))
                            ) AS temperatureAfterDrop
                        FROM IndustrialFacility LEFT JOIN CoolingSystem ON CoolingSystem.FacilityId = IndustrialFacility.IndustrialFacilityId
                        GROUP BY IndustrialFacility.IndustrialFacilityId;""";
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                while (resultSet.next()) {
                    Long entityId = resultSet.getLong("IndustrialFacilityId");
                    String entityName = resultSet.getString("IndustrialFacilityName");
                    BigDecimal temperatureBefore = resultSet.getBigDecimal("IndustrialFacilityTemperature");
                    Integer numberOfSystems = resultSet.getInt("numberOfSystems");
                    BigDecimal sumOfPrices = resultSet.getBigDecimal("sumOfPrices");
                    BigDecimal sumOfElectricityUsage = resultSet.getBigDecimal("sumOfElectricityUsage");
                    BigDecimal sumOfTemperatureDrop = resultSet.getBigDecimal("sumOfTemperatureDrop");
                    BigDecimal temperatureAfter = resultSet.getBigDecimal("temperatureAfterDrop");
                    facilities.add(new Facility(entityId, entityName, temperatureBefore, sumOfPrices, sumOfElectricityUsage, sumOfTemperatureDrop, temperatureAfter, numberOfSystems));
                }

            }
        } catch (Exception e) {
            System.out.println("There was a problem connecting to database.");
        }
        return facilities;
    }

    public static void insertNewManufacturer(Manufacturer manufacturer) {
        try (Connection connection = connectingToDatabase()) {
            if (connection != null) {
                System.out.println("Connected to database.");
                String query = """
                        INSERT INTO Manufacturer (ManufacturerName)
                        VALUES (?) ;""";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, manufacturer.getManufacturerName());
                preparedStatement.executeUpdate();
            }
        } catch (Exception e) {
            System.out.println("There was a problem connecting to database.");
        }
    }

    public static void insertNewCoolingSystem(CoolingSystem coolingSystem) {
        try (Connection connection = connectingToDatabase()) {
            System.out.println("Connected to database.");
            String query = """
                    INSERT INTO CoolingSystem (CoolingSystemName, ManufacturerId, FacilityId, CoolingSystemPrice, CoolingSystemElectricityUsage, CoolingSystemTemperatureDrop, CoolingSystemOnOrOff)
                    VALUES (?, ?, ?, ?, ?, ?, ?);""";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, coolingSystem.getCoolingSystemName());
            preparedStatement.setLong(2, coolingSystem.getManufacturerId());
            preparedStatement.setLong(3, coolingSystem.getFacilityId());
            preparedStatement.setBigDecimal(4, coolingSystem.getPrice());
            preparedStatement.setBigDecimal(5, coolingSystem.getElectricityUsage());
            preparedStatement.setBigDecimal(6, coolingSystem.getTemperatureDrop());
            preparedStatement.setBoolean(7, coolingSystem.getStatus());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            System.out.println("There was a problem connecting to database.");
        }
    }

    public static void insertNewFacility(Facility facility) {
        try (Connection connection = connectingToDatabase()) {
            System.out.println("Connected to database.");
            String query = """
                    INSERT INTO IndustrialFacility (IndustrialFacilityName, IndustrialFacilityTemperature)
                    VALUES (?, ?);""";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, facility.getName());
            preparedStatement.setBigDecimal(2, facility.getTemperatureBefore());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            System.out.println("There was a problem connecting to database.");
        }
    }

    public static void updateManufacturer(Manufacturer manufacturer, Long id) {
        try (Connection connection = connectingToDatabase()) {
            if (connection != null) {
                System.out.println("Connected to database.");
                String query = """
                        UPDATE Manufacturer
                        SET ManufacturerName = ?
                        WHERE ManufacturerId = ? ;""";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, manufacturer.getManufacturerName());
                preparedStatement.setLong(2, id);
                preparedStatement.executeUpdate();

            }
        } catch (Exception e) {
            System.out.println("There was a problem connecting to database.");
        }
    }

    public static void updateCoolingSystem(CoolingSystem coolingSystem, Long id) {
        try (Connection connection = connectingToDatabase()) {
            if (connection != null) {
                System.out.println("Connected to database.");
                String query = """
                        UPDATE CoolingSystem
                        SET CoolingSystemName = ?,
                            ManufacturerId = ?,
                            FacilityId = ?,
                            CoolingSystemPrice = ?,
                            CoolingSystemElectricityUsage = ?,
                            CoolingSystemTemperatureDrop = ?,
                            CoolingSystemOnOrOff = ?
                        WHERE CoolingSystemId = ? ;""";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, coolingSystem.getCoolingSystemName());
                preparedStatement.setLong(2, coolingSystem.getManufacturerId());
                preparedStatement.setLong(3, coolingSystem.getFacilityId());
                preparedStatement.setBigDecimal(4, coolingSystem.getPrice());
                preparedStatement.setBigDecimal(5, coolingSystem.getElectricityUsage());
                preparedStatement.setBigDecimal(6, coolingSystem.getTemperatureDrop());
                preparedStatement.setBoolean(7, coolingSystem.getStatus());
                preparedStatement.setLong(8, id);
                preparedStatement.executeUpdate();

            }
        } catch (Exception e) {
            System.out.println("There was a problem connecting to database.");
        }
    }

    public static void updateFacility(Facility facility, Long id) {
        try (Connection connection = connectingToDatabase()) {
            if (connection != null) {
                System.out.println("Connected to database.");
                String query = """
                        UPDATE IndustrialFacility
                        SET IndustrialFacilityName = ?,
                            IndustrialFacilityTemperature = ?
                        WHERE IndustrialFacilityId = ? ;""";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, facility.getName());
                preparedStatement.setBigDecimal(2, facility.getTemperatureBefore());
                preparedStatement.setLong(3, id);
                preparedStatement.executeUpdate();

            }
        } catch (Exception e) {
            System.out.println("There was a problem connecting to database.");
        }

    }

    public static void deleteManufacturer(Long id) {
        try (Connection connection = connectingToDatabase()) {
            if (connection != null) {
                System.out.println("Connected to database.");
                String query = """
                        DELETE FROM CoolingSystem WHERE ManufacturerId = ? ;
                        DELETE FROM Manufacturer WHERE ManufacturerId = ? ;""";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setLong(1, id);
                preparedStatement.setLong(2, id);
                preparedStatement.executeUpdate();
            }
        } catch (Exception e) {
            System.out.println("There was a problem connecting to database.");
        }
    }

    public static void deleteCoolingSystem(Long id) {
        try (Connection connection = connectingToDatabase()) {
            if (connection != null) {
                System.out.println("Connected to database.");
                String query = "DELETE FROM CoolingSystem WHERE CoolingSystemId = ? ";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (Exception e) {
            System.out.println("There was a problem connecting to database.");
        }
    }

    public static void deleteFacility(Long id) {
        try (Connection connection = connectingToDatabase()) {
            if (connection != null) {
                System.out.println("Connected to database.");
                String query = """
                        DELETE FROM CoolingSystem WHERE FacilityId = ? ;
                        DELETE FROM IndustrialFacility WHERE IndustrialFacilityId = ? ;""";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setLong(1, id);
                preparedStatement.setLong(2, id);
                preparedStatement.executeUpdate();
            }
        } catch (Exception e) {
            System.out.println("There was a problem connecting to database.");
        }
    }

}
