package com.example.industrial_cooling_management_app.sorters;

import com.example.industrial_cooling_management_app.entities.Facility;

import java.util.Comparator;

public class SortingFacilities implements Comparator<Facility> {
    @Override
    public int compare(Facility o1, Facility o2) {
        return o2.getNumberOfUnits().compareTo(o1.getNumberOfUnits());
    }
}
