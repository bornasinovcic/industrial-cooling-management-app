package com.example.industrial_cooling_management_app.sorters;

import com.example.industrial_cooling_management_app.entities.Id;
import com.example.industrial_cooling_management_app.generics.Changes;

import java.util.Comparator;

public class SortingChanges implements Comparator<Changes<Id>> {
    @Override
    public int compare(Changes<Id> o1, Changes<Id> o2) {
        return o2.getLocalDateTime().compareTo(o1.getLocalDateTime());
    }
}
