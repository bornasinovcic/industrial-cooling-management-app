package com.example.industrial_cooling_management_app.sorters;

import com.example.industrial_cooling_management_app.entities.CoolingSystem;

import java.util.Comparator;

public class SortingCoolingSystems implements Comparator<CoolingSystem> {
    @Override
    public int compare(CoolingSystem o1, CoolingSystem o2) {
        return o2.getPrice().compareTo(o1.getPrice());
    }
}
