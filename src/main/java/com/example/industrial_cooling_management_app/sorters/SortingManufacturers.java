package com.example.industrial_cooling_management_app.sorters;

import com.example.industrial_cooling_management_app.entities.Manufacturer;

import java.util.Comparator;

public class SortingManufacturers implements Comparator<Manufacturer> {
    @Override
    public int compare(Manufacturer m0, Manufacturer m1) {
        return m1.getNumberOfSoldUnits().compareTo(m0.getNumberOfSoldUnits());
    }
}
