package com.example.industrial_cooling_management_app.controllers;


import com.example.industrial_cooling_management_app.entities.Id;
import com.example.industrial_cooling_management_app.entities.Role;
import com.example.industrial_cooling_management_app.entities.User;
import com.example.industrial_cooling_management_app.exceptions.CheckBoxNotSelectedException;
import com.example.industrial_cooling_management_app.exceptions.DuplicateNameException;
import com.example.industrial_cooling_management_app.exceptions.WrongPasswordException;
import com.example.industrial_cooling_management_app.generics.Changes;
import com.example.industrial_cooling_management_app.threads.Deserialization;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.industrial_cooling_management_app.entities.PasswordGenerator.randomPasswordGenerator;
import static com.example.industrial_cooling_management_app.files.FilesHandling.addNewUser;
import static com.example.industrial_cooling_management_app.files.FilesHandling.getUsers;
import static com.example.industrial_cooling_management_app.records.Hash.encryption;

public class MainController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);
    public static final int SCREEN_WIDTH = 900, SCREEN_HEIGHT = 500;
    public static User USER;
    private static int length;
    @FXML
    private CheckBox numbers, lowerCase, upperCase;
    @FXML
    private ComboBox<Role> roleComboBox;
    @FXML
    private ComboBox<User> userComboBox;
    @FXML
    private Label passLength;
    @FXML
    private TextField userName;
    @FXML
    private PasswordField userPassword, logInPassword;
    @FXML
    private Slider passSlider;

    private List<User> userList = new ArrayList<>();

    @FXML
    private void signIn() {
        try {
            User logedInUser = userComboBox.getValue();
            String passwordLogIn = logInPassword.getText();

            StringBuilder stringBuilder = new StringBuilder();

            if (logedInUser == null) stringBuilder.append("You did not choose a user.\n");
            if (passwordLogIn.isEmpty()) stringBuilder.append("You did not input a passwordLogIn.\n");

            if (stringBuilder.isEmpty()) {
                List<User> list = getUsers();
                for (User user : list) {
                    if (user.getEntityId().equals(logedInUser.getEntityId())) {
                        isPasswordCorrect(user, passwordLogIn);
                        LOGGER.info("Successfully logged in as -> " + logedInUser.getUserName());
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Logged in");
                        alert.setHeaderText("Successfully logged in as -> " + logedInUser.getUserName());
                        alert.showAndWait();
                        userComboBox.valueProperty().set(null);
                        logInPassword.clear();
                        USER = logedInUser;
                    }
                }
            } else {
                LOGGER.error("Please fill out empty fields.");
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error alert message");
                alert.setHeaderText("Please fill out empty fields.");
                alert.setContentText(stringBuilder.toString());
                alert.showAndWait();
            }
        } catch (WrongPasswordException | IOException e) {
            LOGGER.error(e.getMessage());
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error in user authentication.");
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        }
    }

    private void isPasswordCorrect(User user, String passwordLogIn) {
        String passwordLogInHashed = encryption(passwordLogIn);
        if (!user.getUserPassword().equals(passwordLogInHashed))
            throw new WrongPasswordException("Wrong password for user " + user.getUserName() + ".");
    }


    @FXML
    private void signUp() {
        try {
            List<User> list = getUsers();
            Long newEntityId = list.isEmpty() ? 1L : list.getLast().getEntityId() + 1;
            String newUserName = userName.getText();
            String newUserPassword = userPassword.getText();
            Role newUserRole = roleComboBox.getValue();

            StringBuilder stringBuilder = new StringBuilder();
            if (newUserName.isEmpty()) stringBuilder.append("You forgot to input name for that user.\n");
            if (newUserPassword.isEmpty()) stringBuilder.append("You forgot to input password for that user.\n");
            if (newUserRole == null) stringBuilder.append("You forgot to input role for that user.\n");

            testForDuplicateUserName(list, newUserName);

            if (stringBuilder.isEmpty()) {
                User newUser = new User.UserBuilder()
                        .setEntityId(newEntityId)
                        .setUserName(newUserName)
                        .setUserPassword(encryption(newUserPassword))
                        .setRole(newUserRole)
                        .createUser();
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("New user added.");
                alert.setHeaderText("Information about new user.");
                String string = "Id -> [" + newEntityId + "]" +
                        "\nName -> [" + newUserName + "]" +
                        "\nPassword -> [" + newUserPassword + "]" +
                        "\nRole -> [" + newUserRole + "]";
                alert.setContentText(string);
                Optional<ButtonType> buttonType = alert.showAndWait();
                if (buttonType.isPresent() && buttonType.get() == ButtonType.OK) {
                    list.add(newUser);
                    addNewUser(list);
                    userName.clear();
                    userPassword.clear();
                    roleComboBox.valueProperty().set(null);
                    initialize();
                }
            } else {
                LOGGER.error("Please fill out empty fields.");
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error alert message");
                alert.setHeaderText("Please fill out empty fields");
                alert.setContentText(stringBuilder.toString());
                alert.showAndWait();
            }
        } catch (DuplicateNameException e) {
            LOGGER.error(e.getMessage());
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error alert message");
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        } catch (IOException e) {
            System.out.println("Problem with opening/closing files.");
            throw new RuntimeException(e);
        }
    }

    private void testForDuplicateUserName(List<User> list, String itemName) throws DuplicateNameException {
        for (User user : list)
            if (user.getUserName().equals(itemName))
                throw new DuplicateNameException("This username already exists.");
    }


    @FXML
    private void generatePassword() {
        try {
            userPassword.setText(randomPasswordGenerator(upperCase, lowerCase, numbers, length));
        } catch (CheckBoxNotSelectedException e) {
            LOGGER.error(e.getMessage());
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(e.getMessage());
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        }
    }

    @FXML
    private void initialize() {
        Deserialization deserialization = new Deserialization();
        List<Changes<Id>> changesList = deserialization.deserialize();

        try {
            userComboBox.getItems().removeAll(userList);
            roleComboBox.getItems().removeAll(Role.values());
            userList = getUsers();
            userComboBox.getItems().addAll(userList);
            roleComboBox.getItems().addAll(Role.values());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        length = (int) passSlider.getValue();
        passLength.setText(Integer.toString(length));
        passSlider.valueProperty().addListener((observableValue, number, t1) -> {
            length = (int) passSlider.getValue();
            passLength.setText(Integer.toString(length));
        });
    }
}