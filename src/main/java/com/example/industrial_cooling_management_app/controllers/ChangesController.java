package com.example.industrial_cooling_management_app.controllers;

import com.example.industrial_cooling_management_app.entities.*;
import com.example.industrial_cooling_management_app.generics.Changes;
import com.example.industrial_cooling_management_app.sorters.SortingChanges;
import com.example.industrial_cooling_management_app.threads.Deserialization;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ChangesController {
    @FXML
    private TableView<Changes<Id>> tableView;
    @FXML
    private TableColumn<Changes<Id>, Id> before;
    @FXML
    private TableColumn<Changes<Id>, Id> after;
    @FXML
    private TableColumn<Changes<Id>, User> user;
    @FXML
    private TableColumn<Changes<Id>, String> localDateTime;

    private List<Changes<Id>> list = new ArrayList<>();
    @FXML
    private void initialize() {
        list.clear();
        Deserialization deserialization = new Deserialization();
        list = deserialization.deserialize();
        list.sort(new SortingChanges());
        before.setCellValueFactory(changesIdCellDataFeatures -> {
            if (changesIdCellDataFeatures.getValue().getBefore().getClass() == Manufacturer.class) {
                return new SimpleObjectProperty<>(changesIdCellDataFeatures.getValue().getBefore());
            }
            if (changesIdCellDataFeatures.getValue().getBefore().getClass() == CoolingSystem.class) {
                return new SimpleObjectProperty<>(changesIdCellDataFeatures.getValue().getBefore());
            }
            if (changesIdCellDataFeatures.getValue().getBefore().getClass() == Facility.class) {
                return new SimpleObjectProperty<>(changesIdCellDataFeatures.getValue().getBefore());
            }
            return null;
        });
        after.setCellValueFactory(changesIdCellDataFeatures -> {
            if (changesIdCellDataFeatures.getValue().getBefore().getClass() == Manufacturer.class) {
                return new SimpleObjectProperty<>(changesIdCellDataFeatures.getValue().getAfter());
            }
            if (changesIdCellDataFeatures.getValue().getBefore().getClass() == CoolingSystem.class) {
                return new SimpleObjectProperty<>(changesIdCellDataFeatures.getValue().getAfter());
            }
            if (changesIdCellDataFeatures.getValue().getBefore().getClass() == Facility.class) {
                return new SimpleObjectProperty<>(changesIdCellDataFeatures.getValue().getAfter());
            }
            return null;
        });
        user.setCellValueFactory(changeItemCellDataFeatures -> new SimpleObjectProperty<>(changeItemCellDataFeatures.getValue().getUser()));
        localDateTime.setCellValueFactory(changeLocalDateTimeCellDataFeatures -> new SimpleObjectProperty<>(changeLocalDateTimeCellDataFeatures.getValue().getLocalDateTime().format(DateTimeFormatter.ofPattern("dd.MM.yyy HH:mm"))));
        tableView.setItems(FXCollections.observableList(list));
    }
}
