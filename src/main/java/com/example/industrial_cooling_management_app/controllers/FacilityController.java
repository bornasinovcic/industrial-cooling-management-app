package com.example.industrial_cooling_management_app.controllers;

import com.example.industrial_cooling_management_app.entities.*;
import com.example.industrial_cooling_management_app.exceptions.LoggedInUserNotAdminException;
import com.example.industrial_cooling_management_app.exceptions.NoLoggedInUserException;
import com.example.industrial_cooling_management_app.exceptions.NothingSelectedException;
import com.example.industrial_cooling_management_app.sorters.SortingFacilities;
import com.example.industrial_cooling_management_app.threads.Serialization;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.industrial_cooling_management_app.controllers.MainController.USER;
import static com.example.industrial_cooling_management_app.database.DatabaseHandling.*;

public class FacilityController {
    private static final Logger LOGGER = LoggerFactory.getLogger(FacilityController.class);
    @FXML
    private TextField textField, textFieldName, textFieldTemperature;
    @FXML
    private TableColumn<Facility, Long> tableColumnId;
    @FXML
    private TableColumn<Facility, String> tableColumnName;
    @FXML
    private TableColumn<Facility, Integer> tableColumnUnits;
    @FXML
    private TableColumn<Facility, BigDecimal> tableColumnSumOfPrices, tableColumnSumOfElectricity, tableColumnSumOfTempDrop, tableColumnTempBefore, tableColumnTempAfter;
    @FXML
    private TableView<Facility> facilityTableView;
    private static List<Facility> facilities = new ArrayList<>();

    @FXML
    private void filterOnKeyTyped() {
        String s = textField.getText().toLowerCase();
        List<Facility> list = new ArrayList<>(facilities);
        list = list.stream().filter(facility -> facility.getName().toLowerCase().contains(s)).toList();
        facilityTableView.setItems(FXCollections.observableList(list));
    }

    @FXML
    private void onInsertButtonClick() {
        try {
            isUserLoggedIn(USER);
            isUserAdmin(USER);

            StringBuilder stringBuilder = new StringBuilder();

            if (textFieldName.getText().isEmpty()) stringBuilder.append("You forgot to input name of that facility.\n");
            if (textFieldTemperature.getText().isEmpty())
                stringBuilder.append("You forgot to input temperature of that facility.\n");

            if (stringBuilder.isEmpty()) {
                String name = textFieldName.getText();
                BigDecimal temperature = new BigDecimal(textFieldTemperature.getText());
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("New facility added");
                alert.setHeaderText("Information about new facility.");
                String string = "Name -> [" + name + "]" + "\ntemperature -> [" + temperature + "]";
                alert.setContentText(string);
                Optional<ButtonType> buttonType = alert.showAndWait();
                if (buttonType.isPresent() && buttonType.get() == ButtonType.OK) {
                    insertNewFacility(new Facility(null, name, temperature, null, null, null, null, null));
                    textFieldName.clear();
                    textFieldTemperature.clear();
                    initialize();
                }
            } else {
                LOGGER.error("Please fill out empty fields.");
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error alert message");
                alert.setHeaderText("Please fill out empty fields.");
                alert.setContentText(stringBuilder.toString());
                alert.showAndWait();
            }
        } catch (LoggedInUserNotAdminException | NoLoggedInUserException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            LOGGER.error(e.getMessage());
            alert.setTitle(e.getMessage());
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        } catch (NumberFormatException e) {
            LOGGER.warn("Some of the fields have a wrong data type.");
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning alert message");
            alert.setHeaderText("Some of the fields have a wrong data type.\n" +
                    "Please make sure you have inputted everything correctly.");
            alert.showAndWait();
        }
    }

    @FXML
    private void onUpdateButtonClick() {
        try {
            isUserLoggedIn(USER);
            isUserAdmin(USER);

            String name = textFieldName.getText();
            String temperature = textFieldTemperature.getText();

            Facility selectedFacility = facilityTableView.getSelectionModel().getSelectedItem();
            isSelectedObjectNull(selectedFacility);

            Facility updatedFacility = new Facility(selectedFacility.getEntityId());
            updatedFacility.setName(name.isEmpty() ? selectedFacility.getName() : name);
            updatedFacility.setTemperatureBefore(temperature.isEmpty() ? selectedFacility.getTemperatureBefore() : new BigDecimal(temperature));

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Updating facility");
            alert.setHeaderText("Selected facility will be updated.");
            Optional<ButtonType> buttonType = alert.showAndWait();

            if (buttonType.isPresent() && buttonType.get() == ButtonType.OK) {
                Serialization.serialize(selectedFacility, updatedFacility, USER);
                updateFacility(updatedFacility, selectedFacility.getEntityId());
                LOGGER.info("Facility updated.");
                textFieldName.clear();
                textFieldTemperature.clear();
                initialize();
            }

        } catch (NothingSelectedException | LoggedInUserNotAdminException | NoLoggedInUserException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            LOGGER.error(e.getMessage());
            alert.setTitle(e.getMessage());
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        } catch (NumberFormatException e) {
            LOGGER.warn("Some of the fields have a wrong data type.");
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning alert message");
            alert.setHeaderText("Some of the fields have a wrong data type.\n" +
                    "Please make sure you have inputted everything correctly.");
            alert.showAndWait();
        }

    }

    @FXML
    private void onDeleteButtonClick() {
        try {
            isUserLoggedIn(USER);
            isUserAdmin(USER);
            Facility selectedFacility = facilityTableView.getSelectionModel().getSelectedItem();
            isSelectedObjectNull(selectedFacility);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Deletion of item");
            alert.setHeaderText("Selected facility will be deleted.");
            Optional<ButtonType> buttonType = alert.showAndWait();
            if (buttonType.isPresent() && buttonType.get() == ButtonType.OK) {
                deleteFacility(selectedFacility.getEntityId());
                LOGGER.info("Facility deleted.");
                initialize();
            }
        } catch (NothingSelectedException | LoggedInUserNotAdminException | NoLoggedInUserException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            LOGGER.error(e.getMessage());
            alert.setTitle(e.getMessage());
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        }
    }

    private void isUserLoggedIn(User user) throws NoLoggedInUserException {
        if (user == null) throw new NoLoggedInUserException("No user is logged in.");
    }

    private void isUserAdmin(User user) throws LoggedInUserNotAdminException {
        if (user.getRole().equals(Role.USER)) throw new LoggedInUserNotAdminException("Logged in user is not admin.");
    }

    private void isSelectedObjectNull(Facility selectedItem) {
        if (selectedItem == null) throw new NothingSelectedException("You did not select an object in table view.");
    }


    @FXML
    private void initialize() {
        facilities.clear();
        facilities = getAllFacilities();
        facilities.sort(new SortingFacilities());
        tableColumnId.setCellValueFactory(facilityLongCellDataFeatures -> new SimpleObjectProperty<>(facilityLongCellDataFeatures.getValue().getEntityId()));
        tableColumnName.setCellValueFactory(facilityStringCellDataFeatures -> new SimpleStringProperty(facilityStringCellDataFeatures.getValue().getName()));
        tableColumnUnits.setCellValueFactory(facilityIntegerCellDataFeatures -> new SimpleObjectProperty<>(facilityIntegerCellDataFeatures.getValue().getNumberOfUnits()));
        tableColumnSumOfPrices.setCellValueFactory(facilityBigDecimalCellDataFeatures -> new SimpleObjectProperty<>(facilityBigDecimalCellDataFeatures.getValue().getSumoOfPrices()));
        tableColumnSumOfElectricity.setCellValueFactory(facilityBigDecimalCellDataFeatures -> new SimpleObjectProperty<>(facilityBigDecimalCellDataFeatures.getValue().getSumOfElectricityUsage()));
        tableColumnSumOfTempDrop.setCellValueFactory(facilityBigDecimalCellDataFeatures -> new SimpleObjectProperty<>(facilityBigDecimalCellDataFeatures.getValue().getSumOfTemperatureDrop()));
        tableColumnTempBefore.setCellValueFactory(facilityBigDecimalCellDataFeatures -> new SimpleObjectProperty<>(facilityBigDecimalCellDataFeatures.getValue().getTemperatureBefore()));
        tableColumnTempAfter.setCellValueFactory(facilityBigDecimalCellDataFeatures -> new SimpleObjectProperty<>(facilityBigDecimalCellDataFeatures.getValue().getTemperatureAfter()));
        facilityTableView.setItems(FXCollections.observableList(facilities));
    }
}
