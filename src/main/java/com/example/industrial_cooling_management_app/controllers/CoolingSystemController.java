package com.example.industrial_cooling_management_app.controllers;

import com.example.industrial_cooling_management_app.entities.*;
import com.example.industrial_cooling_management_app.exceptions.LoggedInUserNotAdminException;
import com.example.industrial_cooling_management_app.exceptions.NoLoggedInUserException;
import com.example.industrial_cooling_management_app.exceptions.NothingSelectedException;
import com.example.industrial_cooling_management_app.sorters.SortingCoolingSystems;
import com.example.industrial_cooling_management_app.threads.Serialization;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.industrial_cooling_management_app.controllers.MainController.USER;
import static com.example.industrial_cooling_management_app.database.DatabaseHandling.*;

public class CoolingSystemController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ManufacturerController.class);
    public ToggleGroup status;
    @FXML
    private TextField textField, textFieldName, coolingSystemPrice, coolingSystemElectricityUsage, coolingSystemTempDrop;
    @FXML
    private ComboBox<Manufacturer> manufacturerComboBox;
    @FXML
    private ComboBox<Facility> facilityComboBox;
    @FXML
    private RadioButton buttonTrue, buttonFalse;
    @FXML
    private TableColumn<CoolingSystem, Long> tableColumnId, tableColumnManufacturerId, tableColumnFacilityId;
    @FXML
    private TableColumn<CoolingSystem, String> tableColumnName;
    @FXML
    private TableColumn<CoolingSystem, BigDecimal> tableColumnPrice, tableColumnElectricity, tableColumnTempDrop;
    @FXML
    private TableColumn<CoolingSystem, Boolean> tableColumnStatus;
    @FXML
    private TableView<CoolingSystem> coolingSystemTableView;


    private static List<CoolingSystem> coolingSystemList = new ArrayList<>();

    List<Manufacturer> manufacturerList = getAllManufacturers();
    List<Facility> facilityList = getAllFacilities();


    @FXML
    private void filterOnKeyTyped() {
        String s = textField.getText().toLowerCase();
        List<CoolingSystem> list = new ArrayList<>(coolingSystemList);
        list = list.stream().filter(coolingSystem -> coolingSystem.getCoolingSystemName().toLowerCase().contains(s)).toList();
        coolingSystemTableView.setItems(FXCollections.observableList(list));
    }

    @FXML
    private void onInsertButtonClick() {
        try {
            isUserLoggedIn(USER);
            isUserAdmin(USER);

            StringBuilder stringBuilder = getStringBuilder();

            if (stringBuilder.isEmpty()) {
                String coolingSystemName = textFieldName.getText();
                Manufacturer manufacturer = manufacturerComboBox.getValue();
                String manufacturerId = String.valueOf(manufacturer.getEntityId());
                Facility facility = facilityComboBox.getValue();
                String facilityId = String.valueOf(facility.getEntityId());
                String price = coolingSystemPrice.getText();
                String electricityUsage = coolingSystemElectricityUsage.getText();
                String temperatureDrop = coolingSystemTempDrop.getText();
                boolean status = buttonTrue.isSelected();
                if (buttonFalse.isSelected()) status = false;

                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("New cooling system added");
                alert.setHeaderText("Information about new cooling system.");
                String string = "Name -> [" + coolingSystemName + "]" +
                        "\nManufacturer id -> [" + manufacturerId + "]" +
                        "\nFacility id -> [" + facilityId + "]" +
                        "\nPrice -> [" + price + "]" +
                        "\nElectricity usage -> [" + electricityUsage + "]" +
                        "\nTemperature drop -> [" + temperatureDrop + "]" +
                        "\nStatus -> [" + status + "]";
                alert.setContentText(string);

                Optional<ButtonType> buttonType = alert.showAndWait();
                if (buttonType.isPresent() && buttonType.get() == ButtonType.OK) {
                    insertNewCoolingSystem(new CoolingSystem(null, coolingSystemName, Long.valueOf(manufacturerId), Long.valueOf(facilityId), new BigDecimal(price), new BigDecimal(electricityUsage), new BigDecimal(temperatureDrop), status));
                    LOGGER.info("Cooling system updated.");
                    manufacturerComboBox.valueProperty().set(null);
                    facilityComboBox.valueProperty().set(null);
                    textFieldName.clear();
                    coolingSystemPrice.clear();
                    coolingSystemElectricityUsage.clear();
                    coolingSystemTempDrop.clear();
                    buttonTrue.setSelected(false);
                    buttonFalse.setSelected(false);
                    initialize();
                }
            } else {
                LOGGER.error("Please fill out empty fields.");
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error alert message");
                alert.setHeaderText("Please fill out empty fields.");
                alert.setContentText(stringBuilder.toString());
                alert.showAndWait();
            }
        } catch (LoggedInUserNotAdminException | NoLoggedInUserException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            LOGGER.error(e.getMessage());
            alert.setTitle(e.getMessage());
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        } catch (NumberFormatException e) {
            LOGGER.warn("Some of the fields have a wrong data type.");
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning alert message");
            alert.setHeaderText("Some of the fields have a wrong data type.\n" +
                    "Please make sure you have inputted everything correctly.");
            alert.showAndWait();
        }


    }

    private StringBuilder getStringBuilder() {
        StringBuilder stringBuilder = new StringBuilder();
        if (textFieldName.getText().isEmpty()) stringBuilder.append("You forgot to input name of that system.\n");
        if (manufacturerComboBox.getValue() == null)
            stringBuilder.append("You forgot to input manufacturer of that system.\n");
        if (facilityComboBox.getValue() == null)
            stringBuilder.append("You forgot to input facility of that system.\n");
        if (coolingSystemPrice.getText().isEmpty())
            stringBuilder.append("You forgot to input price of that system.\n");
        if (coolingSystemElectricityUsage.getText().isEmpty())
            stringBuilder.append("You forgot to input electricity usage of that system.\n");
        if (coolingSystemTempDrop.getText().isEmpty())
            stringBuilder.append("You forgot to input temperature drop of that system.\n");
        if (!buttonTrue.isSelected() && !buttonFalse.isSelected())
            stringBuilder.append("You forgot to input status of that system.\n");
        return stringBuilder;
    }

    @FXML
    private void onUpdateButtonClick() {
        try {
            isUserLoggedIn(USER);
            isUserAdmin(USER);

            CoolingSystem selectedCoolingSystem = coolingSystemTableView.getSelectionModel().getSelectedItem();
            isSelectedObjectNull(selectedCoolingSystem);

            Manufacturer manufacturer = manufacturerComboBox.getValue();
            Facility facility = facilityComboBox.getValue();

            String manufacturerId = manufacturer == null ? String.valueOf(selectedCoolingSystem.getManufacturerId()) : String.valueOf(manufacturer.getEntityId());
            String facilityId = facility == null ? String.valueOf(selectedCoolingSystem.getFacilityId()) : String.valueOf(facility.getEntityId());
            String coolingSystemName = textFieldName.getText();
            String price = coolingSystemPrice.getText();
            String electricityUsage = coolingSystemElectricityUsage.getText();
            String temperatureDrop = coolingSystemTempDrop.getText();

            CoolingSystem updatedCoolingSystem = new CoolingSystem(selectedCoolingSystem.getEntityId());
            updatedCoolingSystem.setCoolingSystemName(coolingSystemName.isEmpty() ? selectedCoolingSystem.getCoolingSystemName() : coolingSystemName);
            updatedCoolingSystem.setManufacturerId(Long.valueOf(manufacturerId));
            updatedCoolingSystem.setFacilityId(Long.valueOf(facilityId));
            updatedCoolingSystem.setPrice(price.isEmpty() ? selectedCoolingSystem.getPrice() : new BigDecimal(price));
            updatedCoolingSystem.setElectricityUsage(electricityUsage.isEmpty() ? selectedCoolingSystem.getElectricityUsage() : new BigDecimal(electricityUsage));
            updatedCoolingSystem.setTemperatureDrop(temperatureDrop.isEmpty() ? selectedCoolingSystem.getTemperatureDrop() : new BigDecimal(temperatureDrop));
            if (buttonTrue.isSelected()) {
                updatedCoolingSystem.setStatus(true);
            } else if (buttonFalse.isSelected()) {
                updatedCoolingSystem.setStatus(false);
            } else {
                updatedCoolingSystem.setStatus(selectedCoolingSystem.getStatus());
            }
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Updating cooling system");
            alert.setHeaderText("Selected cooling system will be updated.");
            Optional<ButtonType> buttonType = alert.showAndWait();
            if (buttonType.isPresent() && buttonType.get() == ButtonType.OK) {
                Serialization.serialize(selectedCoolingSystem, updatedCoolingSystem, USER);
                updateCoolingSystem(updatedCoolingSystem, selectedCoolingSystem.getEntityId());
                LOGGER.info("Cooling system updated.");
                manufacturerComboBox.valueProperty().set(null);
                facilityComboBox.valueProperty().set(null);
                textFieldName.clear();
                coolingSystemPrice.clear();
                coolingSystemElectricityUsage.clear();
                coolingSystemTempDrop.clear();
                buttonTrue.setSelected(false);
                buttonFalse.setSelected(false);
                initialize();
            }
        } catch (NothingSelectedException | LoggedInUserNotAdminException | NoLoggedInUserException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            LOGGER.error(e.getMessage());
            alert.setTitle(e.getMessage());
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        } catch (NumberFormatException e) {
            LOGGER.warn("Some of the fields have a wrong data type.");
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning alert message");
            alert.setHeaderText("Some of the fields have a wrong data type.\n" +
                    "Please make sure you have inputted everything correctly.");
            alert.showAndWait();
        }

    }

    @FXML
    private void onDeleteButtonClick() {
        try {
            isUserLoggedIn(USER);
            isUserAdmin(USER);
            CoolingSystem selectedCoolingSystem = coolingSystemTableView.getSelectionModel().getSelectedItem();
            isSelectedObjectNull(selectedCoolingSystem);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Deletion of cooling system");
            alert.setHeaderText("Selected cooling system will be deleted.");
            Optional<ButtonType> buttonType = alert.showAndWait();
            if (buttonType.isPresent() && buttonType.get() == ButtonType.OK) {
                deleteCoolingSystem(selectedCoolingSystem.getEntityId());
                LOGGER.info("Cooling system deleted.");
                initialize();
            }
        } catch (NothingSelectedException | LoggedInUserNotAdminException | NoLoggedInUserException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            LOGGER.error(e.getMessage());
            alert.setTitle(e.getMessage());
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        }
    }

    private void isUserLoggedIn(User user) throws NoLoggedInUserException {
        if (user == null) throw new NoLoggedInUserException("No user is logged in.");
    }

    private void isUserAdmin(User user) throws LoggedInUserNotAdminException {
        if (user.getRole().equals(Role.USER)) throw new LoggedInUserNotAdminException("Logged in user is not admin.");
    }

    private void isSelectedObjectNull(CoolingSystem selectedItem) {
        if (selectedItem == null) throw new NothingSelectedException("You did not select an object in table view.");
    }


    @FXML
    private void initialize() {
        manufacturerComboBox.getItems().removeAll(manufacturerList);
        facilityComboBox.getItems().removeAll(facilityList);
        manufacturerComboBox.getItems().addAll(manufacturerList);
        facilityComboBox.getItems().addAll(facilityList);

        coolingSystemList.clear();
        coolingSystemList = getAllCoolingSystem();
        coolingSystemList.sort(new SortingCoolingSystems());
        tableColumnId.setCellValueFactory(coolingSystemLongCellDataFeatures -> new SimpleObjectProperty<>(coolingSystemLongCellDataFeatures.getValue().getEntityId()));
        tableColumnName.setCellValueFactory(coolingSystemStringCellDataFeatures -> new SimpleStringProperty(coolingSystemStringCellDataFeatures.getValue().getCoolingSystemName()));
        tableColumnManufacturerId.setCellValueFactory(coolingSystemLongCellDataFeatures -> new SimpleObjectProperty<>(coolingSystemLongCellDataFeatures.getValue().getManufacturerId()));
        tableColumnFacilityId.setCellValueFactory(coolingSystemLongCellDataFeatures -> new SimpleObjectProperty<>(coolingSystemLongCellDataFeatures.getValue().getFacilityId()));
        tableColumnPrice.setCellValueFactory(coolingSystemBigDecimalCellDataFeatures -> new SimpleObjectProperty<>(coolingSystemBigDecimalCellDataFeatures.getValue().getPrice()));
        tableColumnElectricity.setCellValueFactory(coolingSystemBigDecimalCellDataFeatures -> new SimpleObjectProperty<>(coolingSystemBigDecimalCellDataFeatures.getValue().getElectricityUsage()));
        tableColumnTempDrop.setCellValueFactory(coolingSystemBigDecimalCellDataFeatures -> new SimpleObjectProperty<>(coolingSystemBigDecimalCellDataFeatures.getValue().getTemperatureDrop()));
        tableColumnStatus.setCellValueFactory(coolingSystemBooleanCellDataFeatures -> new SimpleObjectProperty<>(coolingSystemBooleanCellDataFeatures.getValue().getStatus()));
        coolingSystemTableView.setItems(FXCollections.observableList(coolingSystemList));
    }
}
