package com.example.industrial_cooling_management_app.controllers;

import com.example.industrial_cooling_management_app.main.Main;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

import java.io.IOException;

import static com.example.industrial_cooling_management_app.controllers.MainController.SCREEN_WIDTH;
import static com.example.industrial_cooling_management_app.controllers.MainController.SCREEN_HEIGHT;

public class MenuBarController {
    public void showLogInSingUpScreen() {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("mainScreen.fxml"));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(), SCREEN_WIDTH, SCREEN_HEIGHT);
        } catch (IOException e) {
            System.out.println("Error in showing mainScreen.fxml screen.");
        }
        Main.getStage().setTitle("Manufacturer screen");
        Main.getStage().setScene(scene);
        Main.getStage().show();

    }    public void showManufacturerScreen() {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("manufacturerScreen.fxml"));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(), SCREEN_WIDTH, SCREEN_HEIGHT);
        } catch (IOException e) {
            System.out.println("Error in showing manufacturerScreen.fxml screen.");
        }
        Main.getStage().setTitle("Manufacturer screen");
        Main.getStage().setScene(scene);
        Main.getStage().show();

    }
    public void showCoolingSystemScreen() {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("coolingSystemScreen.fxml"));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(), SCREEN_WIDTH, SCREEN_HEIGHT);
        } catch (IOException e) {
            System.out.println("Error in showing coolingSystemScreen.fxml screen.");
        }
        Main.getStage().setTitle("Cooling System screen");
        Main.getStage().setScene(scene);
        Main.getStage().show();

    }
    public void showFacilityScreen() {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("facilityScreen.fxml"));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(), SCREEN_WIDTH, SCREEN_HEIGHT);
        } catch (IOException e) {
            System.out.println("Error in showing facilityScreen.fxml screen.");
        }
        Main.getStage().setTitle("Facility screen");
        Main.getStage().setScene(scene);
        Main.getStage().show();

    }
    public void showChangesScreen() {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("changesScreen.fxml"));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(), SCREEN_WIDTH, SCREEN_HEIGHT);
        } catch (IOException e) {
            System.out.println("Error in showing changesScreen.fxml screen.");
        }
        Main.getStage().setTitle("Changes screen");
        Main.getStage().setScene(scene);
        Main.getStage().show();

    }
}
