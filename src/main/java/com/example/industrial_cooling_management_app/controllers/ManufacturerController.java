package com.example.industrial_cooling_management_app.controllers;

import com.example.industrial_cooling_management_app.entities.Manufacturer;
import com.example.industrial_cooling_management_app.entities.Role;
import com.example.industrial_cooling_management_app.entities.User;
import com.example.industrial_cooling_management_app.exceptions.LoggedInUserNotAdminException;
import com.example.industrial_cooling_management_app.exceptions.NoLoggedInUserException;
import com.example.industrial_cooling_management_app.exceptions.NothingSelectedException;
import com.example.industrial_cooling_management_app.sorters.SortingManufacturers;
import com.example.industrial_cooling_management_app.threads.Serialization;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.industrial_cooling_management_app.controllers.MainController.USER;
import static com.example.industrial_cooling_management_app.database.DatabaseHandling.*;

public class ManufacturerController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ManufacturerController.class);
    @FXML
    private TextField textField,
            textFieldName;
    @FXML
    private TableColumn<Manufacturer, Long> tableColumnId;
    @FXML
    private TableColumn<Manufacturer, String> tableColumnName;
    @FXML
    private TableColumn<Manufacturer, Integer> tableColumnSold;
    @FXML
    private TableView<Manufacturer> tableViewManufacturer;


    private static List<Manufacturer> manufacturerList = new ArrayList<>();

    @FXML
    private void filterOnKeyTyped() {
        String s = textField.getText().toLowerCase();
        List<Manufacturer> list = new ArrayList<>(manufacturerList);
        list = list.stream()
                .filter(food -> food.getManufacturerName().toLowerCase().contains(s))
                .toList();
        tableViewManufacturer.setItems(FXCollections.observableList(list));
    }

    @FXML
    private void onInsertButtonClick() {
        try {
            isUserLoggedIn(USER);
            isUserAdmin(USER);

            if (!textFieldName.getText().isEmpty()) {
                String manufacturerName = textFieldName.getText();
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("New manufacturer added");
                alert.setHeaderText("Information about new manufacturer.");
                String string = "Name -> [" + manufacturerName + "]";
                alert.setContentText(string);

                Optional<ButtonType> buttonType = alert.showAndWait();
                if (buttonType.isPresent() && buttonType.get() == ButtonType.OK) {
                    insertNewManufacturer(new Manufacturer(null, manufacturerName, null));
                    textFieldName.clear();
                    initialize();
                }
            } else {
                LOGGER.error("Please fill out empty fields.");
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error alert message");
                alert.setHeaderText("Please fill out empty fields.");
                alert.setContentText("You forgot to input name of that manufacturer.");
                alert.showAndWait();
            }

        } catch (LoggedInUserNotAdminException | NoLoggedInUserException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            LOGGER.error(e.getMessage());
            alert.setTitle(e.getMessage());
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        }
    }

    @FXML
    private void onUpdateButtonClick() {
        try {
            isUserLoggedIn(USER);
            isUserAdmin(USER);

            Manufacturer selectedManufacturer = tableViewManufacturer.getSelectionModel().getSelectedItem();
            isSelectedObjectNull(selectedManufacturer);

            String manufacturerName = textFieldName.getText();
            Manufacturer updatedManufacturer = new Manufacturer(null, manufacturerName, null);
            updatedManufacturer.setEntityId(selectedManufacturer.getEntityId());
            updatedManufacturer.setManufacturerName(manufacturerName.isEmpty() ? selectedManufacturer.getManufacturerName() : manufacturerName);

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Updating manufacturer");
            alert.setHeaderText("Selected manufacturer will be updated.");
            Optional<ButtonType> buttonType = alert.showAndWait();

            if (buttonType.isPresent() && buttonType.get() == ButtonType.OK) {
                Serialization.serialize(selectedManufacturer, updatedManufacturer, USER);
                updateManufacturer(updatedManufacturer, selectedManufacturer.getEntityId());
                LOGGER.info("Cooling system updated.");
                textFieldName.clear();
                initialize();
            }

        } catch (NothingSelectedException | LoggedInUserNotAdminException | NoLoggedInUserException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            LOGGER.error(e.getMessage());
            alert.setTitle(e.getMessage());
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        }
    }


    @FXML
    private void onDeleteButtonClick() {
        try {
            isUserLoggedIn(USER);
            isUserAdmin(USER);
            Manufacturer selectedManufacturer = tableViewManufacturer.getSelectionModel().getSelectedItem();
            isSelectedObjectNull(selectedManufacturer);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Deletion of item");
            alert.setHeaderText("Selected manufacturer will be deleted.");
            Optional<ButtonType> buttonType = alert.showAndWait();
            if (buttonType.isPresent() && buttonType.get() == ButtonType.OK) {
                deleteManufacturer(selectedManufacturer.getEntityId());
                LOGGER.info("Manufacturer deleted.");
                initialize();
            }
        } catch (NothingSelectedException | LoggedInUserNotAdminException | NoLoggedInUserException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            LOGGER.error(e.getMessage());
            alert.setTitle(e.getMessage());
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        }
    }

    private void isUserLoggedIn(User user) throws NoLoggedInUserException {
        if (user == null)
            throw new NoLoggedInUserException("No user is logged in.");
    }

    private void isUserAdmin(User user) throws LoggedInUserNotAdminException {
        if (user.getRole().equals(Role.USER))
            throw new LoggedInUserNotAdminException("Logged in user is not admin.");
    }

    private void isSelectedObjectNull(Manufacturer selectedItem) {
        if (selectedItem == null)
            throw new NothingSelectedException("You did not select an object in table view.");
    }

    @FXML
    private void initialize() {
        manufacturerList.clear();
        manufacturerList = getAllManufacturers();
        manufacturerList.sort(new SortingManufacturers());
        tableColumnId.setCellValueFactory(manufacturerLongCellDataFeatures -> new SimpleObjectProperty<>(manufacturerLongCellDataFeatures.getValue().getEntityId()));
        tableColumnName.setCellValueFactory(manufacturerStringCellDataFeatures -> new SimpleStringProperty(manufacturerStringCellDataFeatures.getValue().getManufacturerName()));
        tableColumnSold.setCellValueFactory(manufacturerLongCellDataFeatures -> new SimpleObjectProperty<>(manufacturerLongCellDataFeatures.getValue().getNumberOfSoldUnits()));
        tableViewManufacturer.setItems(FXCollections.observableList(manufacturerList));
    }
}
