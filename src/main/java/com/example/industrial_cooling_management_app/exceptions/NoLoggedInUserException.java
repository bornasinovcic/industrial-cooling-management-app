package com.example.industrial_cooling_management_app.exceptions;

public class NoLoggedInUserException extends Exception {
    public NoLoggedInUserException() {
    }

    public NoLoggedInUserException(String message) {
        super(message);
    }

    public NoLoggedInUserException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoLoggedInUserException(Throwable cause) {
        super(cause);
    }

    public NoLoggedInUserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
