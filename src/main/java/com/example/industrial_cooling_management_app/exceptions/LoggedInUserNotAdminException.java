package com.example.industrial_cooling_management_app.exceptions;

public class LoggedInUserNotAdminException extends Exception {
    public LoggedInUserNotAdminException() {
    }

    public LoggedInUserNotAdminException(String message) {
        super(message);
    }

    public LoggedInUserNotAdminException(String message, Throwable cause) {
        super(message, cause);
    }

    public LoggedInUserNotAdminException(Throwable cause) {
        super(cause);
    }

    public LoggedInUserNotAdminException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
