package com.example.industrial_cooling_management_app.exceptions;

public class CheckBoxNotSelectedException extends RuntimeException {
    public CheckBoxNotSelectedException() {
    }

    public CheckBoxNotSelectedException(String message) {
        super(message);
    }

    public CheckBoxNotSelectedException(String message, Throwable cause) {
        super(message, cause);
    }

    public CheckBoxNotSelectedException(Throwable cause) {
        super(cause);
    }

    public CheckBoxNotSelectedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
