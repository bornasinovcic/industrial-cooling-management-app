package com.example.industrial_cooling_management_app.exceptions;

public class NothingSelectedException extends RuntimeException {
    public NothingSelectedException() {
    }

    public NothingSelectedException(String message) {
        super(message);
    }

    public NothingSelectedException(String message, Throwable cause) {
        super(message, cause);
    }

    public NothingSelectedException(Throwable cause) {
        super(cause);
    }

    public NothingSelectedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
