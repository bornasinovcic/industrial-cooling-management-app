package com.example.industrial_cooling_management_app.threads;

import com.example.industrial_cooling_management_app.entities.Id;
import com.example.industrial_cooling_management_app.generics.Changes;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class Deserialization implements Runnable {
    private static final String PATH_NAME = "files/changes.ser";
    private static List<Changes<Id>> list = new ArrayList<>();
    @Override
    public void run() {

    }
    public synchronized List<Changes<Id>> deserialize() {
        try (FileInputStream file = new FileInputStream(PATH_NAME);
             ObjectInputStream in = new ObjectInputStream(file)) {
            list = (List<Changes<Id>>) in.readObject();
            System.out.println(PATH_NAME + " deserialized");
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("No data inputted yet in list.");
        }
        notifyAll();
        return list;
    }

}
