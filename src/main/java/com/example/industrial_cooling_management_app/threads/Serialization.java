package com.example.industrial_cooling_management_app.threads;

import com.example.industrial_cooling_management_app.entities.Id;
import com.example.industrial_cooling_management_app.entities.User;
import com.example.industrial_cooling_management_app.generics.Changes;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Serialization implements Runnable {
    private static final String PATH_NAME = "files/changes.ser";
    private static List<Changes<Id>> list = new ArrayList<>();

    @Override
    public void run() {

    }
    public static synchronized void serialize(Id selectedItem, Id newMadeItem, User user) {
        Deserialization deserialization = new Deserialization();
        list = deserialization.deserialize();
        try (FileOutputStream file = new FileOutputStream(PATH_NAME);
             ObjectOutputStream out = new ObjectOutputStream(file)) {
            list.add(new Changes<>(selectedItem, newMadeItem, user, LocalDateTime.now()));
            out.writeObject(list);
            System.out.println(PATH_NAME + " serialized");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
