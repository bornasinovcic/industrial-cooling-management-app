package com.example.industrial_cooling_management_app.files;

import com.example.industrial_cooling_management_app.entities.Role;
import com.example.industrial_cooling_management_app.entities.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FilesHandling {
    public static List<User> getUsers() throws IOException {
        List<User> list = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader("files/users.txt"));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String userName = bufferedReader.readLine();
            String userPassword = bufferedReader.readLine();
            Role role = Role.valueOf(bufferedReader.readLine());
            list.add(new User.UserBuilder()
                    .setEntityId(Long.valueOf(line))
                    .setUserName(userName)
                    .setUserPassword(userPassword)
                    .setRole(role)
                    .createUser());
        }
        return list;
    }


    public static void addNewUser(List<User> list) throws IOException {
        FileWriter fileWriter = new FileWriter("files/users.txt");
        PrintWriter printWriter = new PrintWriter(fileWriter);
        for (User user : list) {
            printWriter.println(user.getEntityId());
            printWriter.println(user.getUserName());
            printWriter.println(user.getUserPassword());
            printWriter.println(user.getRole());
        }
        printWriter.flush();
    }

}
