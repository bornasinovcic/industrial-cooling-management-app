module com.example.demo {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires com.h2database;
    requires org.slf4j;

    exports com.example.industrial_cooling_management_app.main;
    opens com.example.industrial_cooling_management_app.main to javafx.fxml;
    exports com.example.industrial_cooling_management_app.controllers;
    opens com.example.industrial_cooling_management_app.controllers to javafx.fxml;
}